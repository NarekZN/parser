<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>parser</title>
    <link rel="shortcut icon" href="/icon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="style.css">
</head> 
<body>
    
    <div class="container">

        <div class="form">
            <input class="url-area" type="text" placeholder="url" >
            <button class="submit">submit</button>
        </div>

    </div>


    <script src="jquery.js"></script>
    <script src="script.js"></script>
</body>
</html>