<?php 

  class Bot {

    public $connection;
    public $hrefsArr = array();

    public function __construct ($serverName,$username,$password,$database) {
      $this->connection = mysqli_connect($serverName,$username,$password,$database); 
    }

    public function __destruct() {
      $this->connection->close();
    }

    private function isInternal($link, $url) {

      $url_parts  = parse_url($url);
      $link_parts = parse_url($link);
      if (isset($link_parts['host'])) {
          if ($link_parts['host'] == $url_parts['host']) {
              return substr($link, 0, 2) == '//' ? $url_parts['scheme'] . ':' . $link : $link;
          }
      } else if ($link_parts['path'][0] == '/' && strlen($link_parts['path']) > 1) {
          return $url_parts['scheme'] . '://' . $url_parts['host'] . $link;
      }
      return false;
    }

    public function finder($link){
      
      if (!in_array($link, $this->hrefsArr)) {
        $this->hrefsArr[] = $link;
      }

      $ch = curl_init($link);

        
      curl_setopt_array($ch, array(
        CURLOPT_FOLLOWLOCATION => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
          
      ));

            
      $page = curl_exec($ch);

      curl_close($ch);
      
      $dom = new DOMDocument();
      @$dom->loadHTML($page);
  
      $links = $dom->getElementsByTagName('a');
      
      
      foreach($links as $href){
        $linkHref = $href->getAttribute('href');
        $internalHref = $this->isInternal($linkHref,$link);
        
        if ($internalHref && !in_array($internalHref,$this->hrefsArr) && $internalHref != $link) {
          array_push($this->hrefsArr, $internalHref);
          $linkText = $href->nodeValue;
          $now = date("Y-m-d H:i:s"); 
          mysqli_query($this->connection, "INSERT INTO `link` (`referrer`, `href`, `content`, `created`) VALUES ('$link', '$internalHref', '$linkText', '$now')");
          $this->finder($internalHref);
        }
      }
    }


    private function get($ref) {
      return $this->connection->query("SELECT * FROM link WHERE link.referrer = '$ref'");
    }

    public function find($ref,$num = 0,$level = 0) {
      if ($result = $this->get($ref)) {
        while ($row = $result->fetch_assoc()) {
          echo str_repeat("&nbsp;", $level); 
          echo "<a href='#'>".$row['href']."</a>" . "<br>"; 
          $this->find($row['href'],2 * ($num + 1), $level+1);
        }
      }
    }


  }
  
  
?>